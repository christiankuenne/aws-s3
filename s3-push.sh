#!/bin/bash

# Show help
if [ $# -ne 6 ]; then
  script="${0##*/}"
  echo "USAGE: $script AWS_REGION S3_BUCKET_NAME AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY LOCAL_SOURCE_PATH S3_DESTINATION_PATH"
  echo "Example: $script eu-central-1 my-bucket my-access-key my-secret /path/of/example.txt ./example.txt"
  exit 1;
fi

# Parse Arguments
AWS_REGION="$1"
AWS_S3_BUCKET_NAME="$2"
AWS_ACCESS_KEY_ID="$3"
AWS_SECRET_ACCESS_KEY="$4"
AWS_S3_STORAGE_CLASS='STANDARD'

# Source Path (local)
sourcePath="$5"

# Destination Path (S3): URL-Encode, but preserve the forward slash
# For example "/path/file with spaces.txt" will be "/path/file%20with%20spaces.txt"
destinationPath=`echo $(urlencode "$6") | sed 's/%2F/\//g'`
if [[ "$destinationPath" != /* ]]; then  
  destinationPath="/$destinationPath"  
fi

# Get Signature
awsStringSign4() {
  kSecret="AWS4$1"
  kDate=$(printf         '%s' "$2" | openssl dgst -sha256 -hex -mac HMAC -macopt "key:${kSecret}"     2>/dev/null | sed 's/^.* //')
  kRegion=$(printf       '%s' "$3" | openssl dgst -sha256 -hex -mac HMAC -macopt "hexkey:${kDate}"    2>/dev/null | sed 's/^.* //')
  kService=$(printf      '%s' "$4" | openssl dgst -sha256 -hex -mac HMAC -macopt "hexkey:${kRegion}"  2>/dev/null | sed 's/^.* //')
  kSigning=$(printf 'aws4_request' | openssl dgst -sha256 -hex -mac HMAC -macopt "hexkey:${kService}" 2>/dev/null | sed 's/^.* //')
  signedString=$(printf  '%s' "$5" | openssl dgst -sha256 -hex -mac HMAC -macopt "hexkey:${kSigning}" 2>/dev/null | sed 's/^.* //')
  printf '%s' "${signedString}"
}

# Initialize helper variables
authType='AWS4-HMAC-SHA256'
host="${AWS_S3_BUCKET_NAME}.s3.${AWS_REGION}.amazonaws.com"
dateValueS=$(date -u +'%Y%m%d')
dateValueL=$(date -u +'%Y%m%dT%H%M%SZ')
if hash file 2>/dev/null; then
  contentType="$(file -b --mime-type "${sourcePath}")"
else
  contentType='application/octet-stream'
fi

# Hash the file to be uploaded
if [ -f "${sourcePath}" ]; then
  payloadHash=$(openssl dgst -sha256 -hex < "${sourcePath}" 2>/dev/null | sed 's/^.* //')
else
  echo "File not found: '${sourcePath}'"
  exit 1
fi

# Create canonical request
# NOTE: order significant in ${headerList} and ${canonicalRequest}
headerList='content-type;host;x-amz-content-sha256;x-amz-date;x-amz-server-side-encryption;x-amz-storage-class'

canonicalRequest="\
PUT
${destinationPath}

content-type:${contentType}
host:${host}
x-amz-content-sha256:${payloadHash}
x-amz-date:${dateValueL}
x-amz-server-side-encryption:AES256
x-amz-storage-class:${AWS_S3_STORAGE_CLASS}

${headerList}
${payloadHash}"

# Hash it
canonicalRequestHash=$(printf '%s' "${canonicalRequest}" | openssl dgst -sha256 -hex 2>/dev/null | sed 's/^.* //')

# Create string to sign
stringToSign="\
${authType}
${dateValueL}
${dateValueS}/${AWS_REGION}/s3/aws4_request
${canonicalRequestHash}"

# Sign the string
signature=$(awsStringSign4 "${AWS_SECRET_ACCESS_KEY}" "${dateValueS}" "${AWS_REGION}" 's3' "${stringToSign}")

# Show message
echo "Uploading ${sourcePath} -> ${AWS_S3_BUCKET_NAME} ${AWS_REGION} ${destinationPath} ${AWS_S3_STORAGE_CLASS}"

# Upload
curl -i -L -T "${sourcePath}" \
  -H "Content-Type: ${contentType}" \
  -H "Host: ${host}" \
  -H "X-Amz-Content-SHA256: ${payloadHash}" \
  -H "X-Amz-Date: ${dateValueL}" \
  -H "X-Amz-Server-Side-Encryption: AES256" \
  -H "X-Amz-Storage-Class: ${AWS_S3_STORAGE_CLASS}" \
  -H "Authorization: ${authType} Credential=${AWS_ACCESS_KEY_ID}/${dateValueS}/${AWS_REGION}/s3/aws4_request, SignedHeaders=${headerList}, Signature=${signature}" \
  "https://${host}${destinationPath}"