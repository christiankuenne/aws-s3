install:
	cp s3-get.sh /usr/bin/s3-get
	cp s3-push.sh /usr/bin/s3-push
	chmod +x /usr/bin/s3-get
	chmod +x /usr/bin/s3-push

clean:
	rm /usr/bin/s3-get
	rm /usr/bin/s3-push