#!/bin/bash

# Show help
if [ $# -ne 6 ]; then
  script="${0##*/}"
  echo "USAGE: $script AWS_REGION S3_BUCKET_NAME AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY S3_SOURCE_PATH LOCAL_DESTINATION_PATH"
  echo "Example: $script eu-central-1 my-bucket my-access-key my-secret /path/of/example.txt ./example.txt"
  exit 1;
fi

# Parse Arguments
AWS_REGION="$1"
AWS_S3_BUCKET_NAME="$2"
AWS_ACCESS_KEY_ID="$3"
AWS_SECRET_ACCESS_KEY="$4"

# Source Path (S3): URL-Encode, but preserve the forward slash
# For example "/path/file with spaces.txt" will be "/path/file%20with%20spaces.txt"
sourcePath=`echo $(urlencode "$5") | sed 's/%2F/\//g'`
if [[ "$sourcePath" != /* ]]; then  
  sourcePath="/$sourcePath"  
fi

# Destination Path (local)
destinationPath="$6"

# Get Signature
awsStringSign4() {
  kSecret="AWS4$1"
  kDate=$(printf         '%s' "$2" | openssl dgst -sha256 -hex -mac HMAC -macopt "key:${kSecret}"     2>/dev/null | sed 's/^.* //')
  kRegion=$(printf       '%s' "$3" | openssl dgst -sha256 -hex -mac HMAC -macopt "hexkey:${kDate}"    2>/dev/null | sed 's/^.* //')
  kService=$(printf      '%s' "$4" | openssl dgst -sha256 -hex -mac HMAC -macopt "hexkey:${kRegion}"  2>/dev/null | sed 's/^.* //')
  kSigning=$(printf 'aws4_request' | openssl dgst -sha256 -hex -mac HMAC -macopt "hexkey:${kService}" 2>/dev/null | sed 's/^.* //')
  signedString=$(printf  '%s' "$5" | openssl dgst -sha256 -hex -mac HMAC -macopt "hexkey:${kSigning}" 2>/dev/null | sed 's/^.* //')
  printf '%s' "${signedString}"
}

# Initialize helper variables
authType='AWS4-HMAC-SHA256'
dateValueS=$(date -u +'%Y%m%d')
dateValueL=$(date -u +'%Y%m%dT%H%M%SZ')
contentType='application/octet-stream'

# Payload Hash
payloadHash="$(printf "" | openssl dgst -sha256 | sed 's/^.* //')"

# Create canonical request
# NOTE: order significant in ${headerList} and ${canonicalRequest}
headerList='content-type;host;x-amz-content-sha256;x-amz-date'

canonicalRequest="\
GET
/${AWS_S3_BUCKET_NAME}${sourcePath}

content-type:${contentType}
host:s3.${AWS_REGION}.amazonaws.com
x-amz-content-sha256:${payloadHash}
x-amz-date:${dateValueL}

${headerList}
${payloadHash}"

# Hash it
canonicalRequestHash=$(printf '%s' "${canonicalRequest}" | openssl dgst -sha256 -hex 2>/dev/null | sed 's/^.* //')

# Create string to sign
stringToSign="\
${authType}
${dateValueL}
${dateValueS}/${AWS_REGION}/s3/aws4_request
${canonicalRequestHash}"

# Sign the string
signature=$(awsStringSign4 "${AWS_SECRET_ACCESS_KEY}" "${dateValueS}" "${AWS_REGION}" 's3' "${stringToSign}")

# Show message
echo "Downloading ${AWS_S3_BUCKET_NAME} ${AWS_REGION} ${sourcePath} -> ${destinationPath}"

# Download
curl -L -f -o "${destinationPath}" \
  -H "content-type: ${contentType}" \
  -H "x-amz-content-sha256: ${payloadHash}" \
  -H "x-amz-date: ${dateValueL}" \
  -H "Authorization: ${authType} Credential=${AWS_ACCESS_KEY_ID}/${dateValueS}/${AWS_REGION}/s3/aws4_request, SignedHeaders=${headerList}, Signature=${signature}" \
  "https://s3.${AWS_REGION}.amazonaws.com/${AWS_S3_BUCKET_NAME}{$sourcePath}"