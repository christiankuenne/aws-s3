# Amazon S3 Scripts

Bash scripts to upload/download files to/from Amazon S3.

## Getting started

Create an [IAM user](https://console.aws.amazon.com/iamv2/home#/users). Under **AWS credential type** select **Access key - Programmatic access**. Make a note of the **access key ID** and the **secret access key**.

Create an [S3 bucket](https://s3.console.aws.amazon.com/s3/). Under **Permissions**, edit the **Bucket policy** to grant read/write access to the IAM user:

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "examplePolicy",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::123456789012:user/[iam-user-here]"
            },
            "Action": ["s3:GetObject", "s3:PutObject"],
            "Resource": "arn:aws:s3:::[bucket-name-here]/*"
        }
    ]
}
```

## Installation:

Make sure that _curl_, _openssl_ and _urlencode_ are installed.

```
$ sudo make install
```

## Upload to S3

```
./s3-push.sh AWS_REGION S3_BUCKET_NAME AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY LOCAL_SOURCE_PATH S3_DESTINATION_PATH

Example:
./s3-push.sh eu-central-1 my-bucket my-access-key my-secret /path/of/example.txt ./example.txt
```

## Download from S3

```
./s3-get.sh AWS_REGION S3_BUCKET_NAME AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY S3_SOURCE_PATH LOCAL_DESTINATION_PATH

Example:
./s3-get.sh eu-central-1 my-bucket my-access-key my-secret /path/of/example.txt ./example.txt
```

## Uninstall

```
$ sudo make clean
```
